﻿﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LM_ProcessSalesFileUtility
{
    class Program
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            try
            {
                if (args.Count() == 0)
                {
                    throw new Exception("No arguments given.");
                }

                switch (args[0].ToUpper())
                {
                    case "UPLOAD":
                        ProcessLMSUpload(args);
                        break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Command line samples
        //UPLOAD AO "[F]"
        //UPLOAD ATM "[F]" CSV
        static private void ProcessLMSUpload(string[] args)
        {
            log4net.Config.BasicConfigurator.Configure();
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
            try
            {
                string fileType = args[1];
                string fileformat = args.Count() <= 3 || args[3]==null ? "EXCEL" : args[3];
                string sheetName = string.Empty;
                string tableName = string.Empty;
                string sp_name = string.Empty;
                string fileHeader = string.Empty;
                switch (fileType.ToUpper())
                {
                    case "AO":
                        sheetName = ConfigurationManager.AppSettings["AO_01"];
                        tableName = ConfigurationManager.AppSettings["AO_01_tbl"];
                        sp_name = ConfigurationManager.AppSettings["AO_01_Process_SP"];
                        break;
                    case "HL":
                        sheetName = ConfigurationManager.AppSettings["HL"];
                        tableName = ConfigurationManager.AppSettings["HL_tbl"];
                        sp_name = ConfigurationManager.AppSettings["HL_Process_SP"];
                        break;
                    case "PL":
                        sheetName = ConfigurationManager.AppSettings["PL"];
                        tableName = ConfigurationManager.AppSettings["PL_tbl"];
                        sp_name = ConfigurationManager.AppSettings["PL_Process_SP"];
                        break;
                    case "ATM":
                        tableName = ConfigurationManager.AppSettings["ATM_tbl"];
                        fileHeader = ConfigurationManager.AppSettings["ATM_hdr"];
                        sp_name = ConfigurationManager.AppSettings["ATM_Process_SP"];
                        break;
                }

                string destinationFolder = ConfigurationManager.AppSettings["DestinationFolder"];
                log.Info("Destinatiuon folder " + destinationFolder);
                string fileName = System.IO.Path.GetFileNameWithoutExtension(args[2]) + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + System.IO.Path.GetExtension(args[2]);
                //string fileName = System.IO.Path.GetFileNameWithoutExtension(sourceFile) + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + System.IO.Path.GetExtension(sourceFile);
                log.Info("file name " + fileName);
                System.IO.File.Move(args[2], destinationFolder + fileName);
                log.Info("file moved");
                //System.IO.File.Move(sourceFile, destinationFolder + fileName);
                
                ProcessFile filePr = new ProcessFile();
                log.Info("file processing started");

                //bool result = filePr.UploadDataSheet(destinationFolder, fileName, sheetName, tableName, System.IO.Path.GetExtension(args[2]));
                bool result = false;

                switch (fileformat.ToUpper())
                {
                    case "EXCEL":
                        result = filePr.UploadDataSheet(destinationFolder, fileName, sheetName, tableName, System.IO.Path.GetExtension(args[2]));
                        break;
                    case "CSV":
                        result = filePr.UploadCsvData(destinationFolder, fileName, tableName, fileHeader, System.IO.Path.GetExtension(args[2]));
                        break;
                }
                log.Info("file processing ended");
                if (result)
                {
                    result = filePr.processData(sp_name);
                }
            }
            catch(Exception ex)
            {
                log.Error("Error Message: " + ex.Message.ToString(), ex);
            }
        }

    }

    public class ProcessFile
    {
        //log4net.Config.BasicConfigurator.Configure();  
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        private static readonly string sqlConnectionString = ConfigurationManager.AppSettings["SqlConnString"].ToString();
        public bool UploadDataSheet(string FilePath, string FileName, string SheetName, string TableName, string fileExt)
        {
            log.Info("Upload file");
            bool status = false;
            DataTable dt = ConvertExcelToDataTable(FilePath + "\\" + FileName, SheetName, fileExt);
            // dt.Columns.Add("FileId", typeof(System.Int32));
            //foreach (DataRow dr in dt.Rows)
            //{
            //    dr["FileId"] = FileId;
            //}
            bool _clearData = TruncateTable(TableName);
            log.Info("File truncated");
            log.Info("Bulk insert");
            if (_clearData)
            {
                status = BulkInsert(dt, TableName);
            }
            log.Info("Bulk insert end");
            return status;
        }

        public DataTable ConvertExcelToDataTable(string fileName, string ConfigSheetName, string fileExt)
        {
            string conn = string.Empty;
            DataTable dtResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            if (fileExt.CompareTo(".xls") == 0)
                conn = @"provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HRD=Yes;IMEX=1';"; //for below excel 2007  
            else
                conn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0;HDR=Yes';"; //for above excel 2007  

            using (OleDbConnection objConn = new OleDbConnection(conn))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    DataTable tempDataTable;
                    if ((ConfigSheetName != null) && (ConfigSheetName != ""))
                    {
                        tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where ((dataRow["TABLE_NAME"].ToString().Contains(ConfigSheetName)) &&
                                          (!dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")))
                                         select dataRow).CopyToDataTable();
                    }
                    else
                    {
                        tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    }
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
                return dtResult; //Returning Dattable  
            }
        }

        public bool TruncateTable(string TableName)
        {
            bool truncateStatus = false;
            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    // Delete old entries
                    SqlCommand truncate = new SqlCommand("TRUNCATE TABLE [" + TableName + "]", conn);
                    truncate.ExecuteNonQuery();
                    conn.Close();
                    truncateStatus = true;
                }
            }
            catch (Exception ex)
            {
                //LogLib.Log.LogText(ex.Message, "IncentiveCalcWcfLib", "FileUploaderDAO", "Error");
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return truncateStatus;
        }

        public bool BulkInsert(DataTable dt, string tableName)
        {

            bool status = false;
            SqlBulkCopy bulkCopy = null;

            try
            {
                bulkCopy = new SqlBulkCopy(sqlConnectionString, SqlBulkCopyOptions.TableLock)
                {
                    DestinationTableName = tableName,
                    BatchSize = 100000,
                    BulkCopyTimeout = 360
                };
                bulkCopy.WriteToServer(dt);
                bulkCopy.Close();
                status = true;
            }
            catch (Exception ex)
            {
                //LogLib.Log.LogText(ex.Message, "IncentiveCalcWcfLib", "FileUploaderDAO", "Error");
                throw ex;
            }
            finally
            {
                if (bulkCopy != null)
                {
                    bulkCopy.Close();
                }
            }
            return status;
        }

        public bool processData(string sp_name)
        {
            bool isSuccess = false;
            SqlDataReader dr = null;
            try
            {
                dr = SqlHelper.ExecuteReader(sqlConnectionString, CommandType.StoredProcedure, sp_name);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = true;
                    }
                }
                dr.Close();
            }
            catch(Exception ex)
            {
                log.Error("Error Message: " + ex.Message.ToString(), ex);
                throw ex;
            }
            finally
            {
                if (!dr.IsClosed && dr != null)
                {
                    dr.Close();
                }
            }
            return isSuccess;
        }

        public bool UploadCsvData(string FilePath, string FileName, string TableName, string FileHeader, string fileExt)
        {
            log.Info("Upload file");
            bool status = false;
            DataTable dt = ConvertTextToDataTable(FilePath + "\\" + FileName, ",", FileHeader);
            log.Info("Truncate Table");
            bool _clearData = TruncateTable(TableName);
            if (_clearData)
            {
                log.Info("Bulk Insert");
                status = BulkInsert(dt, TableName);
            }
            return status;
        }

        public DataTable ConvertTextToDataTable(string FileName, string Delimiter, string Header)
        {
            DataTable dtResult = new DataTable();
            if (Delimiter == "\\t")
            {
                Delimiter = "\t";
            }
            using (var sr = new StreamReader(FileName))
            {
                string[] headers = Header.Split(',');
                foreach (string header in headers)
                {
                    dtResult.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rowValues = sr.ReadLine().Split(Delimiter[0]);
                    DataRow dr = dtResult.NewRow();
                    for (int i = 0; i < rowValues.Length; i++)
                    {
                        dr[i] = rowValues[i];
                    }
                    dtResult.Rows.Add(dr);
                }
            }
            return dtResult; //Returning Dattable  
        }

    }
}
